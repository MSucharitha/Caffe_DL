#!/usr/bin/env sh
set -e

TOOLS=./build/tools
start=$SECONDS
echo "start time $start sec"
$TOOLS/caffe train \
  --solver=examples/cifar10/cifar10_quick_solver.prototxt $@

# reduce learning rate by factor of 10 after 8 epochs
$TOOLS/caffe train \
  --solver=examples/cifar10/cifar10_quick_solver_lr1.prototxt \
  --snapshot=examples/cifar10/cifar10_quick_iter_1.solverstate.h5 $@
end=$(($SECONDS-$start))
echo "elapsed time $end  sec"
