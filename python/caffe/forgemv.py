from pynq import MMIO
from pynq import PL
from pynq import Overlay 
Overlay("base_with_5_gemv.bit").download()
def selectmatrix(alpha,A,X,beta,Y,M,N):
    if (M==10 and N==1):
        gemv_I = MMIO(0x83C20000,0x10000)
        add_A = 0x40
        add_X = 0x80
        add_Y = 0xC0
        add_a = 0x10
        add_b = 0x88
        ap_start = 0x00
    elif (M==32 and N==32):
        gemv_I = MMIO(0x83C30000,0x10000)
        add_A = 0x1000
        add_X = 0x2000
        add_Y = 0x2100
        add_a = 0x10
        add_b = 0x2080
        ap_start = 0x0000
    elif (M==32 and N==256):
        gemv_I = MMIO(0x83C40000,0x20000)
        add_A = 0x08000
        add_X = 0x10000
        add_Y = 0x10480
        add_a = 0x00010
        add_b = 0x10400
        ap_start = 0x00000
    elif (M==64 and N==64):
        gemv_I = MMIO(0x83C60000,0x10000)
        add_A = 0x4000
        add_X = 0x8000
        add_Y = 0x8200
        add_a = 0x0010
        add_b = 0x8100
        ap_start = 0x0000
    elif (M==100 and N==10):
        gemv_I = MMIO(0x83C70000,0x10000)
        add_A = 0x1000
        add_X = 0x2000
        add_Y = 0x2100
        add_a = 0x10
        add_b = 0x2080
        ap_start = 0x00
    
    for j in range(0,N):
        gemv_I.write(add_X + 4*j,X[j])
        #print(str(hex(add_X + 4*j)) + "  :  " + str(gemv_I.read(add_X + 4*j)))
    for i in range(0,M * N):              
        gemv_I.write(add_A + 4*i,A[i])
        #print(str(hex(add_A + 4*i)) + "  :  " + str(gemv_I.read(add_A + 4*i)))
        gemv_I.write(add_a,0x02)
        gemv_I.write(add_b,0x00)
        gemv_I.write(ap_start,0x01)
    
    for k in range(0,M):
        Y[k]=gemv_I.read(add_Y + 4*k )
    return Y
    
    
    
