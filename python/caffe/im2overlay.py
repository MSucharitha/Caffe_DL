from pynq import MMIO
from pynq import PL
from pynq import Overlay 
Overlay(u"/home/xilinx/pynq/bitstream/base.bit").download()
#Overlay(u"/home/xilinx/pynq/bitstream/base.bit").ip_dict
class my_im2col:
	"""
    
  	function for im2col_cpu
   
	 """
    
	mmio_im2col = MMIO(0x43C40000,0x10000)
	temp=0
	BASE_ADDRESS_IM2COL = 0x43C40000
	BASE_ADDRESS_IM2COL_OFFSET = 0x10
	def __init__(self):
		self.temp=0
	def inputdata(self,data_im,channels,height,width,kernel_h,kernel_w,pad_h,pad_w,stride_h,stride_w,dilation_h,dilation_w,data_col):
		self.mmio_im2col.write(0x0,1)
		self.mmio_im2col.write(0x10+0x08,channels)
		print("channels")
		self.mmio_im2col.write(0x10+0x10,height)
		self.mmio_im2col.write(0x10+0x18,width)
		self.mmio_im2col.write(0x10+0x20,kernel_h)
		self.mmio_im2col.write(0x10+0x28,kernel_w)
		self.mmio_im2col.write(0x10+0x30,pad_h) 
		self.mmio_im2col.write(0x10+0x38,pad_w)
		self.mmio_im2col.write(0x10+0x40,stride_h)
		self.mmio_im2col.write(0x10+0x48,stride_w)
		self.mmio_im2col.write(0x10+0x50,dilation_h)
		self.mmio_im2col.write(0x10+0x58,dilation_w)
		self.mmio_im2col.write(0x10+0x60,data_col)
		self.mmio_im2col.write(0x10,data_im)
		return 5;
